package com.m1kes.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ControllerExample {

    @RequestMapping("/ControllerExample")
    public String getIndex(){
        return "index";
    }

}
