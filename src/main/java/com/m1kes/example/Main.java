package com.m1kes.example;

import org.springframework.beans.factory.InitializingBean;

public class Main implements InitializingBean {

    //Basically called once on application startup , use this when setting up DB connections ect.. or global data
    @Override
    public void afterPropertiesSet() throws Exception {

    }

}
