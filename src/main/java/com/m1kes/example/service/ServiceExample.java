package com.m1kes.example.service;

import com.m1kes.example.responses.JsonResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ServiceExample {
    @RequestMapping(method = RequestMethod.GET, value = "/ServiceExample")
    public @ResponseBody JsonResponse view(){
        return new JsonResponse("Success","You have successfully viewed a json response example :)");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/TestPostValues")
    public @ResponseBody JsonResponse getResponse(@RequestParam(value = "username" ,required = false) String username ){
        return new JsonResponse("Success","You passed a value through : " + username);
    }

}
