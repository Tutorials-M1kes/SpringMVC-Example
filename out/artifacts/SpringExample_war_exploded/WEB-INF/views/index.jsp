<%--
  Created by IntelliJ IDEA.
  User: Michael
  Date: 2018/08/24
  Time: 11:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home Page </title>

    <p>This is a test of a Spring MVC project using both MVC & Restful Service </p>

    <p>To view the apis use the following url mappings :</p>

    <ul>
        <li>localhost:port/ControllerExample</li>
        <li>localhost:port/ServiceExample</li>
    </ul>

</head>
<body>

</body>
</html>
